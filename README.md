# E-Service：一种方便用户配置负载均衡规则的扩展版Service

众所周知，K8S原生的Service所使用的负载均衡算法只有有限的几种，不能自定义，本项目旨在解决这个问题，为用户提供一种自由控制Service负载均衡算法的方案。

## 原理

本项目基于`kubebuilder`编写了一个名叫`eservice`的CRD（自定义资源）。

`e-service`将负责管理3个子资源：
* 一系列位于`eservice` namespace的headless service，其endpoint配置为要进行负载均衡的目的端pod
* 一个位于`eservice` namespace的转发容器，负责从上述headless service中读取pod的IP并进行转发，其转发规则就是要交给用户自定义的负载均衡算法
* 一个位于应用名称空间内的clusterip service，负责接收源端来的包并将其交给`eservice` namespace内的转发容器
  * 难点：需要实现不同node上的service名称相同但行为不同的功能（每个node上的service都需要将流量发送到位于同一个node上的转发容器中）

在无`e-service`的情况下，一个正常的K8S Service大致是这样的结构：

![无e-service](./_asserts/no-es.svg)

而改用`e-service`后，结构会变成这样：

![无e-service](./_asserts/es.svg)

## 分析

* 这种方法参考自Service Mesh中实现统一转发的流量挟持方法
  * Service Mesh中的流量挟持有两种：
    * 在Daemonset中运行proxy
      * 一个Node上的所有Pod的流量都经过这个proxy中转发
      * 优点是可以基于Node配置转发规则，贴近底层网络，速度比较快
      * 缺点是每当Pod有调入调出时，转发规则都要更新
    * 在Pod中注入Sidecar
      * 一个Pod上的所有流量都经过这个Pod中的proxy转发
      * 优点是配置方便，一个Pod一套转发规则，Pod调度时就会带着转发规则走，更贴近逻辑上的K8S模型
      * 缺点是离底层网络比较远，不方便优化，性能不佳
* 在可预见的将来，`e-service`将和Service Mesh中的proxy方案一样，不可避免地产生性能问题
  * K8S中`service`的负载均衡规则都是linux ipvs自带的，转发到哪里在内核里面就计算完成了
  * ipvs出于性能考虑没有提供动态的负载均衡功能，只能以读写配置的方式修改转发规则，这也是`service`只能支持随机的负载均衡的原因
  * 因此，`e-service`及其他所有支持自定义负载均衡规则的转发方案的性能都不可能超过K8S的`service`的转发性能（延迟、吞吐量等）